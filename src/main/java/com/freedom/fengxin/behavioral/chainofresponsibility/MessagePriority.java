/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/6/16:39
 * 项目名称: DesignPatternLearn
 * 文件名称: MessagePriority.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/

package com.freedom.fengxin.behavioral.chainofresponsibility;

/**
 * 包名称： com.freedom.fengxin.behavioral.chainofresponsibility
 * 类名称：MessagePriority
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/6/16:39
 */
enum MessagePriority {
    Normal,
    High
}


class Message {
    public String text;
    public MessagePriority priority;

    public Message(String text, MessagePriority priority) {
        this.text = text;
        this.priority = priority;
    }


}