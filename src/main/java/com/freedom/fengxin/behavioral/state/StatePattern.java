/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/6/10:02
 * 项目名称: DesignPatternLearn
 * 文件名称: RemoteControl.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.behavioral.state;

/**
 * 包名称：com.freedom.fengxin.behavioral.state
 * 类名称：RemoteControl
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/6/10:02
 */

class TV {
    private RemoteControl state;

    public TV(RemoteControl state) {
        this.state = state;
    }

    public RemoteControl getState() {
        return state;
    }

    public void setState(RemoteControl state) {
        this.state = state;
    }

    public void pressButton() {
        state.pressSwitch(this);
    }
}

abstract class RemoteControl {
   public abstract void pressSwitch(TV tv);
}

class Off extends RemoteControl {
    public void pressSwitch(TV tv) {
        System.out.println("off to on.");
        tv.setState(new On());
    }

}

class On extends RemoteControl {
    public void pressSwitch(TV tv) {
        System.out.println("on to off.");
        tv.setState(new Off());
    }
}


public class StatePattern {
    public static void main(String[] args){
        System.out.println("StatePattern Demo");
        Off initialState = new Off();
        TV tv = new TV(initialState);
        tv.pressButton();
        tv.pressButton();
        tv.pressButton();
        tv.pressButton();
    }
}