/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/16:12
 * 项目名称: DesignPatternLearn
 * 文件名称: ICommand.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/

package com.freedom.fengxin.behavioral.command;

/**
 * 包名称： com.freedom.fengxin.behavioral.command
 * 类名称：ICommand
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/16:12
 */


public interface ICommand {

    void doJob();

}
