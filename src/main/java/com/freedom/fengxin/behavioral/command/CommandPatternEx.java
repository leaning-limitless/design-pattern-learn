/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/16:25
 * 项目名称: DesignPatternLearn
 * 文件名称: CommandPatternEx.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.behavioral.command;

/**
 * 包名称：com.freedom.fengxin.behavioral.command
 * 类名称：CommandPatternEx
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/16:25
 */


public class CommandPatternEx {


    public static void main(String[] args){

        System.out.println("CommandPatternEx");
        Receiver receiver = new Receiver();
        Invoke invoke = new Invoke();

        UndoCommand undoCommand = new UndoCommand(receiver);
        RedoCommand redoCommand = new RedoCommand(receiver);

        invoke.execute(undoCommand);
        invoke.execute(redoCommand);


    }

}
