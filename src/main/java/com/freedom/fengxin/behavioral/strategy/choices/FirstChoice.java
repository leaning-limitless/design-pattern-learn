/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/13:34
 * 项目名称: DesignPatternLearn
 * 文件名称: FirstChoice.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.behavioral.strategy.choices;

import java.awt.*;

/**
 * 包名称：com.freedom.fengxin.behavioral.strategy.choices
 * 类名称：FirstChoice
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/13:34
 */


public class FirstChoice implements IChoice {

    public void myChoice(String str1, String str2) {
        System.out.println("add the numbers");
        System.out.println(Integer.parseInt(str1)+Integer.parseInt(str2));
    }
}
