/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/13:41
 * 项目名称: DesignPatternLearn
 * 文件名称: StrategyPatternEx.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.behavioral.strategy;

import com.freedom.fengxin.behavioral.strategy.choices.FirstChoice;
import com.freedom.fengxin.behavioral.strategy.choices.IChoice;
import com.freedom.fengxin.behavioral.strategy.choices.SecondChoice;

/**
 * 包名称：com.freedom.fengxin.behavioral.strategy.choices
 * 类名称：StrategyPatternEx
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/13:41
 */


public class StrategyPatternEx {
    public static void main(String[] args){

        IChoice choice = new FirstChoice();
        Context context = new Context();
        context.setChoice(choice);
        context.showChoice("12","1");
        choice = new SecondChoice();
        context.setChoice(choice);
        context.showChoice("12","1");

    }
}
