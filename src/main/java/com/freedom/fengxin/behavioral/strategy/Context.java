/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/13:41
 * 项目名称: DesignPatternLearn
 * 文件名称: context.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.behavioral.strategy;

import com.freedom.fengxin.behavioral.strategy.choices.IChoice;

/**
 * 包名称：com.freedom.fengxin.behavioral.strategy
 * 类名称：context
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/13:41
 */


public class Context {
    IChoice choice;

    public void setChoice(IChoice choice) {
        this.choice = choice;
    }

    public void showChoice(String str1,String str2) {
        choice.myChoice(str1,str2);
    }
}
