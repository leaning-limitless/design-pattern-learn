/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/9:59
 * 项目名称: DesignPatternLearn
 * 文件名称: Observer.java
 * 文件描述: @Description: observer
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.behavioral.observer;

/**
 * 包名称：com.freedom.fengxin.creational.observer
 * 类名称：Observer
 * 类描述：observer
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/9:59
 */


public class Observer implements IObserver {

    String name;
    public Observer(String name) {
        this.name = name;
    }

    public void update(String subName,int i){
        System.out.println(name+":flag in subject:" + subName +" changed to "+i+" in Subject");
    }



}
