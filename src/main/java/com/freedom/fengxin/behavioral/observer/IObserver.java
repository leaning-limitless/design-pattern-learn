/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/10:17
 * 项目名称: DesignPatternLearn
 * 文件名称: IObserver.java
 * 文件描述: @Description: IObserver
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/

package com.freedom.fengxin.behavioral.observer;

/**
 * 包名称： com.freedom.fengxin.creational.observer
 * 类名称：IObserver
 * 类描述：IObserver
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/10:17
 */

public interface IObserver {

    void update(String subName,int i);

}
