/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/9:57
 * 项目名称: DesignPatternLearn
 * 文件名称: Subject.java
 * 文件描述: @Description: subject 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 包名称：com.freedom.fengxin.creational.observer
 * 类名称：Subject
 * 类描述：subject
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/9:57
 */


public class Subject implements ISubject {

    private String name;
    private int flag;

    List<Observer> observerList = new ArrayList<Observer>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
        notifyObservers();
    }

    public void register(Observer observer) {
        observerList.add(observer);

    }

    public void unregister(Observer observer) {
        observerList.remove(observer);
    }

    public void notifyObservers() {
        for (Observer observer : observerList) {
            observer.update(name,flag);
        }
    }
}
