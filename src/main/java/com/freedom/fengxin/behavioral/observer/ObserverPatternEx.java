/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/9:56
 * 项目名称: DesignPatternLearn
 * 文件名称: ObserverPatternEx.java
 * 文件描述: @Description: pattern use
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.behavioral.observer;

/**
 * 包名称：com.freedom.fengxin.creational.observer
 * 类名称：ObserverPatternEx
 * 类描述：pattern use
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/9:56
 */


public class ObserverPatternEx {

    public static void main(String[] args){

        Subject subject = new Subject();
        int observerNum = 2;
        for (int i = 0; i < observerNum; i++) {
            subject.register(new Observer(Observer.class.getSimpleName()+i));
        }

        subject.setName("subjectA");
        subject.setFlag(100);

    }

}
