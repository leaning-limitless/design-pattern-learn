/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/13:21
 * 项目名称: DesignPatternLearn
 * 文件名称: TemplateMethodPatternEx.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.behavioral.templatemethod;

/**
 * 包名称：com.freedom.fengxin.behavioral.templatemethod
 * 类名称：TemplateMethodPatternEx
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/13:21
 */


public class TemplateMethodPatternEx {
    public static void main(String[] args){

        BasicEngineering bs = new ComputerScience();

        bs.papers();

        bs = new Electronics();

        bs.papers();

    }
}
