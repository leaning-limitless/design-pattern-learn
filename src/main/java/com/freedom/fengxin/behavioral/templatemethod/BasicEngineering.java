/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/13:13
 * 项目名称: DesignPatternLearn
 * 文件名称: BasicEngineering.java
 * 文件描述: @Description: basic
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.behavioral.templatemethod;

/**
 * 包名称：com.freedom.fengxin.behavioral.templatemethod
 * 类名称：BasicEngineering
 * 类描述：basic
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/13:13
 */


public abstract class BasicEngineering {
    public void papers() {
        math();
        softSkills();
        specialPaper();
    }

    private void math() {
        System.out.println("math");
    }
    private void softSkills() {
        System.out.println("SoftSkills");
    }
    public abstract void specialPaper();
}
