/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/13:18
 * 项目名称: DesignPatternLearn
 * 文件名称: ComputerScience.java
 * 文件描述: @Description: cs
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.behavioral.templatemethod;

/**
 * 包名称：com.freedom.fengxin.behavioral.templatemethod
 * 类名称：ComputerScience
 * 类描述：cs
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/13:18
 */


public class ComputerScience extends BasicEngineering {

    public void specialPaper() {
        System.out.println("OOP");
    }
}


