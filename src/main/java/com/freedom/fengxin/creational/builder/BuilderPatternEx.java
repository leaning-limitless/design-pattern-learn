/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/6/10:32
 * 项目名称: DesignPatternLearn
 * 文件名称: BuilderPatternEx.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.creational.builder;
import java.util.LinkedList;

/**
 * 包名称：com.freedom.fengxin.creational
 * 类名称：BuilderPatternEx
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/6/10:32
 */

class Product {
    private LinkedList<String> parts = new LinkedList<>();

    public void add(String part) {
        parts.add(part);
    }

    public void show(){
        System.out.println("product completed as below.");
        for (int i = 0; i < parts.size(); i++) {
            System.out.println(parts.get(i));
        }
    }
}

interface IBuilder {
    void buildBody();
    void insertWheels();
    void addHeadlights();
    Product getVehicle();
}


class Car implements IBuilder {
    private Product product = new Product();

    @Override
    public void buildBody(){
        product.add("build body of car");
    }

    @Override
    public void insertWheels() {
        product.add("4 wheels are added");
    }

    @Override
    public void addHeadlights() {
        product.add("2 headlights are added");
    }

    @Override
    public Product getVehicle() {
        return product;
    }
}


class MotorCycle implements IBuilder {

    private Product product = new Product();

    @Override
    public void buildBody() {
        product.add("body of motorcycle");
    }

    @Override
    public void insertWheels() {
        product.add("2 wheels are added");
    }

    @Override
    public void addHeadlights() {
        product.add("1 headlights are added");
    }

    @Override
    public Product getVehicle() {
        return product;
    }

}



class Director {

    IBuilder builder;

    public void construct(IBuilder builder) {

        this.builder = builder;
        builder.buildBody();
        builder.insertWheels();
        builder.addHeadlights();



    }


}



public class BuilderPatternEx {

    public static void main(String[] args) {

        Director director = new Director();
        IBuilder carBuilder = new Car();
        IBuilder motorBuilder = new MotorCycle();

        director.construct(carBuilder);
        director.construct(motorBuilder);

        Product p1 = carBuilder.getVehicle();
        p1.show();

        Product p2 = motorBuilder.getVehicle();
        p2.show();



    }
}
