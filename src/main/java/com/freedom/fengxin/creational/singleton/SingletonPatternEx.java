/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/10:38
 * 项目名称: DesignPatternLearn
 * 文件名称: SingletonPatternEx.java
 * 文件描述: @Description: singleton pattern
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.creational.singleton;

/**
 * 包名称：com.freedom.fengxin.creational.singleton
 * 类名称：SingletonPatternEx
 * 类描述：singleton pattern
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/10:38
 */


public class SingletonPatternEx {

    public static void main(String[] args){

        MakeACaptain captain1 = MakeACaptain.getCaptain();
        MakeACaptain captain2 = MakeACaptain.getCaptain();

        if (captain1 == captain2) {
            System.out.println("you have got the same captain.");
        }

    }
}
