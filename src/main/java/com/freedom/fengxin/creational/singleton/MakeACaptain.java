/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/10:33
 * 项目名称: DesignPatternLearn
 * 文件名称: MakeACaptain.java
 * 文件描述: @Description: make a capatin
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.creational.singleton;

/**
 * 包名称：com.freedom.fengxin.creational.singleton
 * 类名称：MakeACaptain
 * 类描述：make a capatin
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/10:33
 */


public class MakeACaptain {

    private static MakeACaptain captain;
    private MakeACaptain() {}
    public static MakeACaptain getCaptain() {
        if (captain == null) {
            captain = new MakeACaptain();
            System.out.println("new captain is maked.");
        } else {
            System.out.println("captain has already existed.");
        }
        return captain;
    }

}
