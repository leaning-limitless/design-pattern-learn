/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/6/8:30
 * 项目名称: DesignPatternLearn
 * 文件名称: FactoryPatternEx.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.creational.factorymethod;

/**
 * 包名称：com.freedom.fengxin.creational.factorymethod
 * 类名称：FactoryPatternEx
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/6/8:30
 */


interface IAnimal {
    void speak();
}

class Duck implements IAnimal {
    public void speak() {
        System.out.println("Duck:Pack");
    }
}

class Tiger implements IAnimal {
    public void speak() {
        System.out.println("Tiger:Halum");
    }
}

abstract class IAnimalFactory {

    public abstract IAnimal
    getAnimalByType(String type) throws Exception;
}



class ConcreteFactory extends IAnimalFactory {
    public IAnimal getAnimalByType(String type) throws Exception {
        switch (type) {

            case "Duck":
                return new Duck();
            case "Tiger":
                return new Tiger();
            default:
                throw new Exception("Unrecognized Animal Type.");
        }
    }
}



// 抽象工厂方法本身也只是 把工厂方法以接口的方式抽象出来，和工厂方法并没有非常大的区别


public class FactoryPatternEx {


    public static void main(String[] args) throws Exception{

        IAnimalFactory animalFactory = new ConcreteFactory();
        IAnimal duck = animalFactory.getAnimalByType("Duck");
        duck.speak();
        IAnimal tiger = animalFactory.getAnimalByType("Tiger");
        tiger.speak();

    }

}
