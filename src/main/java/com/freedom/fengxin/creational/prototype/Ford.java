/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/6/15:59
 * 项目名称: DesignPatternLearn
 * 文件名称: Ford.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.creational.prototype;

/**
 * 包名称：com.freedom.fengxin.creational.prototype
 * 类名称：Ford
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/6/15:59
 */

public class Ford extends BasicCar<Ford> {
    public Ford(String m) {
        modelName = m;
    }
}
