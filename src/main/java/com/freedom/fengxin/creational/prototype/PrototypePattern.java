/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/6/15:42
 * 项目名称: DesignPatternLearn
 * 文件名称: PrototypePattern.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.creational.prototype;

import java.util.Random;

/**
 * 包名称：com.freedom.fengxin.creational.prototype
 * 类名称：PrototypePattern
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/6/15:42
 */



abstract class BasicCar<T extends BasicCar> implements Cloneable {
    String modelName;
    int price;

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public static int getDeltaPrice() {
        Random r = new Random();
        int p = r.nextInt(100000);
        return p;
    }

    public T clone() throws CloneNotSupportedException {
        return (T)super.clone();
    }

}


public class PrototypePattern {

    public static void main(String[] args) throws CloneNotSupportedException {

        BasicCar nano = new Nano("green nano");
        nano.price = 100000;
        BasicCar ford = new Ford("yellow ford");
        ford.price = 500000;

        BasicCar nanoClone = nano.clone();
        nanoClone.price += nanoClone.price+nano.getDeltaPrice();
        System.out.println(nano.price);
        System.out.println(nanoClone.price);

        BasicCar fordClone = ford.clone();
        fordClone.price += fordClone.price+ford.getDeltaPrice();
        System.out.println(ford.price);
        System.out.println(fordClone.price);










    }
}
