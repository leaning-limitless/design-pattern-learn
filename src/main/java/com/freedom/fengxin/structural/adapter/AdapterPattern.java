/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/15:07
 * 项目名称: DesignPatternLearn
 * 文件名称: AdapterPattern.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.adapter;



/**
 * 包名称：com.freedom.fengxin.structural.adapter
 * 类名称：AdapterPattern
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/15:07
 */

abstract class Shape {
}

class Rect extends Shape {
    public double l;
    public double w;

    public Rect(double l, double w) {
        this.l = l;
        this.w = w;
    }
}

class Triangle extends Shape {
    public double b;
    public double h;
    public Triangle(double b, double h) {
        this.b = b;
        this.h = h;
    }
}


class Calculator {
    Shape shape;

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public  double getArea() {
        Double area = 0.0;
        if (shape instanceof Rect) {
            Rect rect = (Rect)shape;
            area = rect.l*rect.w;
        } else if (shape instanceof Triangle) {
            Triangle triangle = (Triangle)shape;
            area = triangle.b*triangle.h*0.5;

        } else {
            throw new RuntimeException("unrecognized shape.");
        }
        System.out.println(shape.getClass().getSimpleName()+" area is "+area);
        return area;
    }
}


class StaticCalculator {

    public static double getArea(Rect shape) {
        Double area = 0.0;
        Rect rect = (Rect)shape;
        area = rect.l*rect.w;
        System.out.println(StaticCalculator.class.getSimpleName()+" "+shape.getClass().getSimpleName()+" area is "+area);
        return area;
    }
}

class StaticCalculatorAdapter {
    public static double getArea(Triangle shape) {
        Double area = 0.0;
        Rect rect = new Rect(shape.b/2,shape.h);
        area = StaticCalculator.getArea(rect);
        System.out.println(StaticCalculatorAdapter.class.getSimpleName()+" "+shape.getClass().getSimpleName()+" area is "+area);
        return area;
    }

}






public class AdapterPattern {

    public static void main(String[] args) {

        Calculator calc = new Calculator();
        Shape shape;

        shape = new Rect(20,10);
        calc.setShape(shape);
        calc.getArea();
        StaticCalculator.getArea((Rect)shape);

        shape = new Triangle(20,10);
        calc.setShape(shape);
        calc.getArea();
        StaticCalculatorAdapter.getArea((Triangle) shape);







    }


}
