/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/12:24
 * 项目名称: DesignPatternLearn
 * 文件名称: DecoratorPatternEx.java
 * 文件描述: @Description: decorator
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.decorator;

/**
 * 包名称：com.freedom.fengxin.structural.decorator
 * 类名称：DecoratorPatternEx
 * 类描述：decorator
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/12:24
 */

abstract class Component {
    public abstract void doJob();
}

abstract class AbstractDecorator extends Component {
    private Component com;

    public void setCom(Component com) {
        this.com = com;
    }

    public void doJob() {
        if (com != null) {
            com.doJob();
        }
    }
}

class ConcreteDecoratorEx1 extends AbstractDecorator {
    public void doJob() {
        super.doJob();
        System.out.println("I am explicitly from Ex1");
    }
}

class ConcreteDecoratorEx2 extends AbstractDecorator {
    @Override
    public void doJob() {
        System.out.println("ConcreteDecoratorEx2 starts");
        super.doJob();
        System.out.println("I am explicitly from Ex2");
        System.out.println("ConcreteDecoratorEx2 ends");
    }
}

class ConcreteComponent extends Component {
    public void doJob() {
        System.out.println("I'm from Concrete Component-I am closed for modification");
    }
}

public class DecoratorPatternEx {
    public static void main(String[] args){
        Component com = new ConcreteComponent();
        ConcreteDecoratorEx1 cd1 = new ConcreteDecoratorEx1();
        cd1.setCom(com);
        ConcreteDecoratorEx2 cd2 = new ConcreteDecoratorEx2();
        cd2.setCom(com);
        cd1.doJob();
        cd2.doJob();
        cd2.setCom(cd1);
        cd2.doJob();
    }
}
