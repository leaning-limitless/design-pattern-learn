/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/6/13:15
 * 项目名称: DesignPatternLearn
 * 文件名称: FlyweightPattern.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.flyweight;

import com.freedom.fengxin.structural.facade.robotfacade.RobotFacade;
import com.freedom.fengxin.structural.facade.robotparts.RobotColor;

import java.util.HashMap;
import java.util.Map;

/**
 * 包名称：com.freedom.fengxin.structural.flyweight
 * 类名称：FlyweightPattern
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/6/13:15
 */


interface IRobot {
    void print();
}

class SmallRobot implements IRobot {

    @Override
    public void print() {
        System.out.println("Small Robot");
    }
}

class LargeRobot implements IRobot {
    @Override
    public void print() {
        System.out.println("Large Robot");
    }
}



class RobotFactory {
    Map<String,IRobot> shapes = new HashMap<>();

    public int totalObjectsCreated() {
        return shapes.size();
    }

    public IRobot getRobotFromFactory(String robotCategory) throws Exception {
        IRobot robot = null;
        if (shapes.containsKey(robotCategory)) {
            robot = shapes.get(robotCategory);
        }else {
            switch (robotCategory) {
                case "small":
                    System.out.println("we do not have small robot, create one");
                    robot = new SmallRobot();
                    shapes.put(robotCategory,robot);
                    break;
                case "large":
                    System.out.println("we do not have large robot, create one");
                    robot = new LargeRobot();
                    shapes.put(robotCategory,robot);
                    break;
                default:
                    throw new RuntimeException("unrecognized shape");
            }
        }

        return robot;
    }

}



public class FlyweightPattern {

    public static void main(String[] args) throws Exception{

        RobotFactory myfactory = new RobotFactory();

        IRobot shape = myfactory.getRobotFromFactory("small");
        shape.print();

        shape = myfactory.getRobotFromFactory("small");
        shape.print();
        System.out.println("distinct robot num = "+myfactory.totalObjectsCreated());

        shape = myfactory.getRobotFromFactory("large");
        shape.print();

        shape = myfactory.getRobotFromFactory("large");
        shape.print();
        System.out.println("distinct robot num = "+myfactory.totalObjectsCreated());

    }



}
