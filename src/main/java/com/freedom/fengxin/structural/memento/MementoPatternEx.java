/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/6/9:35
 * 项目名称: DesignPatternLearn
 * 文件名称: Memento.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.memento;

import javax.swing.*;

/**
 * 包名称：com.freedom.fengxin.structural.memento
 * 类名称：Memento
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/6/9:35
 */


class Memento {
    private String state;
    public Memento(String state) {
        this.state = state;
        System.out.println("memento state set to :"+state);
    }

    public String getState() {
        System.out.println("State at present :"+ state);

        return state;
    }
}

class Originator {
    private String state;
    Memento m;

    public void setState(String state) {
        this.state = state;
        System.out.println("originator state set to :"+state);

    }

    public Memento originateMemento(){
        m = new Memento(state);
        return m;
    }

    public void revert(Memento memento) {
        System.out.println("Restoring to previous state.");
        state = memento.getState();
  }
}

class Caretaker {
    private Memento memento;

    public void saveMemento(Memento m) {
        memento = m;
    }

    public Memento RetrieveMemento() {

        return memento;
    }
}
public class MementoPatternEx {
    public static void main(String[] args){

        Originator o = new Originator();
        o.setState("first state.");

        // holding old state
        Caretaker c = new Caretaker();
        c.saveMemento(o.originateMemento());

        // changing state
        o.setState("second state");

        // restore saved state
        o.revert(c.RetrieveMemento());

    }

}