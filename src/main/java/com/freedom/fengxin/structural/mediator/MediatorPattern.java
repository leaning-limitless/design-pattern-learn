/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/6/14:44
 * 项目名称: DesignPatternLearn
 * 文件名称: MediatorPattern.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.mediator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 包名称：com.freedom.fengxin.structural.mediator
 * 类名称：MediatorPattern
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/6/14:44
 */




abstract class Role {
    protected Mediator mediator;
    public String name;

    public void setName(String name) {
        this.name = name;
    }

    public Role(Mediator mediator, String name) {
        this.mediator = mediator;
        this.name = name;
    }

    public void send(List<Role> receivers,String msg) {
        mediator.send(this,receivers,msg);
    }

    public void notify(String msg) {
        System.out.println(this.name+" get msg :" +msg);
    }
}

abstract class Mediator {
    public abstract void send(Role sender, List<Role> receivers, String msg);
}


class Trader extends Role {

    public Trader(Mediator mediator,String name) {
        super(mediator,name);
    }
}

class Saler extends Role {

    public Saler(Mediator mediator, String name) {
        super(mediator,name);
    }
}

class Boss extends Role {

    public Boss(Mediator mediator, String name) {
        super(mediator, name);
    }

}

class Intern extends Role {
    public Intern(Mediator mediator, String name) {
        super(mediator, name);
    }
}


class CompanyMediator extends Mediator {

    List<Role> authorizedRoles = new ArrayList<>();

    public void authorize(Role role) {
        authorizedRoles.add(role);
    }

    @Override
    public void send(Role role,List<Role> receivers, String msg) {

        if (authorizedRoles.contains(role)) {
            for (Role receiver : receivers) {
                receiver.notify(msg);
            }

        } else {
            System.out.println("Unauthorized Role.");
        }
    }
}


public class MediatorPattern {


    public static void main(String[] args){

        CompanyMediator mediator = new CompanyMediator();

        Trader trader = new Trader(mediator,"trader");
        Saler saler = new Saler(mediator,"saler");
        Boss boss = new Boss(mediator,"boss");

        mediator.authorize(boss);
        mediator.authorize(trader);
        mediator.authorize(saler);

        mediator.send(trader, Arrays.asList(saler),"trade completed.");
        mediator.send(saler, Arrays.asList(trader),"sale completed.");
        mediator.send(boss, Arrays.asList(saler,trader),"work hard.");

    }

}
