/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/10:50
 * 项目名称: DesignPatternLearn
 * 文件名称: ConcreteSubject.java
 * 文件描述: @Description: concrete subject
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.proxy.original;

/**
 * 包名称：com.freedom.fengxin.creational.proxy.original
 * 类名称：ConcreteSubject
 * 类描述：concrete subject
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/10:50
 */


public class ConcreteSubject extends Subject {

    public void doSomeWork() {
        System.out.println("I am from concrete subject.");
    }
}
