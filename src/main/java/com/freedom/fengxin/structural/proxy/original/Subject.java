/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/10:49
 * 项目名称: DesignPatternLearn
 * 文件名称: Subject.java
 * 文件描述: @Description: subject
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.proxy.original;

/**
 * 包名称：com.freedom.fengxin.creational.proxy.original
 * 类名称：Subject
 * 类描述：subject
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/10:49
 */


public abstract class Subject {

    public abstract void doSomeWork();

}
