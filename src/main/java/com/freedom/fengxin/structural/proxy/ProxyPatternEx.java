/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/10:48
 * 项目名称: DesignPatternLearn
 * 文件名称: ProxyPatternEx.java
 * 文件描述: @Description: proxy pattern
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.proxy;

/**
 * 包名称：com.freedom.fengxin.creational.proxy
 * 类名称：ProxyPatternEx
 * 类描述：proxy pattern
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/10:48
 */


public class ProxyPatternEx {

    public static void main(String[] args){

    }

}
