/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/10:51
 * 项目名称: DesignPatternLearn
 * 文件名称: Proxy.java
 * 文件描述: @Description: proxy
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.proxy.proxy;

import com.freedom.fengxin.structural.proxy.original.ConcreteSubject;
import com.freedom.fengxin.structural.proxy.original.Subject;

/**
 * 包名称：com.freedom.fengxin.creational.proxy.proxy
 * 类名称：Proxy
 * 类描述：proxy
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/10:51
 */


public class Proxy extends Subject {

    ConcreteSubject cs;
    public void doSomeWork() {
        System.out.println("Proxy call is happening.");
        if (cs == null) {
            cs = new ConcreteSubject();
        }
        cs.doSomeWork();
    }
}
