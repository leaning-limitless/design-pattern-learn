/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/17:00
 * 项目名称: DesignPatternLearn
 * 文件名称: RobotFacade.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.facade.robotfacade;

import com.freedom.fengxin.structural.facade.robotparts.RobotBody;
import com.freedom.fengxin.structural.facade.robotparts.RobotColor;
import com.freedom.fengxin.structural.facade.robotparts.RobotMetal;

/**
 * 包名称：com.freedom.fengxin.structural.facade.robotfacade
 * 类名称：RobotFacade
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/17:00
 */


public class RobotFacade {
    RobotColor rc;
    RobotMetal rm;
    RobotBody  rb;

    public RobotFacade() {
        rc = new RobotColor();
        rm = new RobotMetal();
        rb = new RobotBody();
    }

    public void constructRobot(String color, String metal) {
        System.out.println("creation of the robot starts.");
        rc.setColor(color);
        rm.setMetal(metal);
        rb.createBody();
        System.out.println("creation of the robot ends.");
        System.out.println();

    }


}
