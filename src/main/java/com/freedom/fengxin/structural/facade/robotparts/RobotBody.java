/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/16:53
 * 项目名称: DesignPatternLearn
 * 文件名称: RobotBody.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.facade.robotparts;

/**
 * 包名称：com.freedom.fengxin.structural.facade.robotparts
 * 类名称：RobotBody
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/16:53
 */


public class RobotBody {
    public void createBody() {
        System.out.println("Body Creation done.");
    }
}
