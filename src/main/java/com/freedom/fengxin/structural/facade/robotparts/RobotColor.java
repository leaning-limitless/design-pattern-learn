/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/16:55
 * 项目名称: DesignPatternLearn
 * 文件名称: RobotColor.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.facade.robotparts;

/**
 * 包名称：com.freedom.fengxin.structural.facade.robotparts
 * 类名称：RobotColor
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/16:55
 */


public class RobotColor {
    private String color;
    public void setColor(String color) {
        this.color = color;
        System.out.println("Color is set to :"+this.color);
    }
}
