/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/16:55
 * 项目名称: DesignPatternLearn
 * 文件名称: RobotMetal.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.facade.robotparts;

/**
 * 包名称：com.freedom.fengxin.structural.facade.robotparts
 * 类名称：RobotMetal
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/16:55
 */


public class RobotMetal {
    private String metal;
    public void setMetal(String metal) {
        this.metal = metal;
        System.out.println("Metal is set to :"+this.metal);
    }
}
