/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/9/5/16:53
 * 项目名称: DesignPatternLearn
 * 文件名称: FacadePatternEx.java
 * 文件描述: @Description: 
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.freedom.fengxin.structural.facade;

import com.freedom.fengxin.structural.facade.robotfacade.RobotFacade;

/**
 * 包名称：com.freedom.fengxin.structural.facade
 * 类名称：FacadePatternEx
 * 类描述：
 * 创建人：@author fengxin
 * 创建时间：2019/9/5/16:53
 */


public class FacadePatternEx {
    public static void main(String[] args){

        RobotFacade rf1 = new RobotFacade();
        rf1.constructRobot("Red","LLDPE");

        RobotFacade rf2 = new RobotFacade();
        rf2.constructRobot("Green","Iron");



    }
}
